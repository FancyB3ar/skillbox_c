#include <iostream>

int main(int argc, char **argv)
{
    int height = 0;
    do
    {
        std::cout << "Введите высоту ёлки: ";
        std::cin >> height;
    } while (height < 0);

    int countSymbolsInLine = (height<<1) - 1;

    for(int i = 1; i <= height; ++i)
    {
        int countSpaces = (i<<1) - 1;
        int countSymbolsBeforeSharp = (countSymbolsInLine - countSpaces) / 2;

        for(int j = 0; j < countSymbolsInLine; ++j)
        {
            if(j < countSymbolsBeforeSharp || 
               j >= countSymbolsBeforeSharp + countSpaces)
            {
                std::cout <<" ";
            }
            else
                std::cout <<"#";
        }

        std::cout << "\n";
    }

    return 0;
}