#include <iostream>

int main(int argc, char **argv)
{
    uint16_t posXmax = 15,
             posYmax = 20;
    
    uint16_t posX = posXmax / 2,
             posY = posYmax / 2;

    char command = '\0';
    
    while(1)
    {
        std::cout << "Марсоход находится на позиции " << posX << ", " << posY << ", введите команду: \n";
        std::cin >> command;

        switch(command)
        {
        case 'w':
            if(posX != 0) --posX;
            break;

        case 'a':
            if(posY != 0) --posY;
            break;
        
        case 's':
            if(posX != posXmax) ++posX;
            break;

        case 'd':
            if(posY != posYmax) ++posY;
            break;

        default:
            break;
        }
    }
    
    return 0;
}