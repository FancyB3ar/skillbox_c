#include <iostream>

int main(int argc, char **argv)
{
    int milk = 0,
        water = 0;
    
    do
    {
        std::cout << "Введите объём молока и воды в кофемашине: ";
        std::cin >> milk >> water;
    } while (milk < 0 || water < 0);

    for(;;)
    {
        uint16_t request = 0;
        do
        {
            std::cout << "Какой напиток Вы хотите? [1 - Американо, 2 - Латте]: ";
            std::cin >> request;
        } while (request < 1 || request > 2);

        if(request == 1)
        {
            if(water < 300)
            {
                std::cerr << "Не хватает воды!\n";
                return 1;
            }
            else
            {
                water -= 300;
                std::cout << "Ваш напиток готов!\n";
            }
        }
        else
        {
            if(water < 30)
            {
                std::cerr << "Не хватает воды!\n";
                return 1;
            }
            else
            {
                if(milk < 270)
                {
                    std::cerr << "Не хватает молока!\n";
                    return 2;
                }
                else
                {
                    water -= 30;
                    milk -= 270;
                    std::cout << "Ваш напиток готов!\n";
                }
            }
        }
    }
    
    return 0;
}