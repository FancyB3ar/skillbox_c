#include <iostream>

int main(int argc, char **argv)
{
    int32_t n = 0, x = 0;
    
    do
    {
        std::cout << "Введите количество бактерий и капель антибиотика в чашке: ";
        std::cin >> n >> x;
    } while (n < 1 || x < 1);
    
    
    for(uint16_t i = 10; i > 0 && n > 0; --i)
    {
        n <<= 1;
        n -= x * i;
        if(n < 0) n = 0;
        std::cout << 11 - i << " ч. В чашке Петри " << n << " бактерий.\n";
    }

    if(n > 0) std::cout << "Антибиотик прекратил своё действие не убив бактерию!\n";

    return 0;
}