#include <iostream>
#include <string>

int main(int argc, char **argv)
{
    std::string name = "";
    
    std::cout << "Введите имя: ";
    std::cin >> name;

    int w = name.size() + 2;
    
    std::cout << "|";
    for(int i = 0; i < w; ++i) std::cout << "-";
    std::cout << "|\n";

    std::cout << "| " << name << " |\n";

    std::cout << "|";
    for(int i = 0; i < w; ++i) std::cout << "-";
    std::cout << "|\n";

    return 0;
}