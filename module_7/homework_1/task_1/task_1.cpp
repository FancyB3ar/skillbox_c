#include <iostream>

int main(int argc, char **argv)
{
    uint16_t countBuckwheat = 100;
    uint16_t countBuckwheatEatenPernMonth = 4;
    
    for(uint16_t i = 1; countBuckwheat > 0; ++i)
    {
        countBuckwheat -= countBuckwheatEatenPernMonth;
        std::cout << "Количество гречки через " << i << " месяцев:" << countBuckwheat << "\n"; 
    }
}