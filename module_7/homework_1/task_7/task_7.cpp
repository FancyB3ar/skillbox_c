#include <iostream>

int main(int argc, char **argv)
{
    uint16_t countX = 20, countY = 50;
    uint16_t halfCountX = countX / 2,
             halfCountY = countY / 2;

    for(int x = -1; x <= countX; ++x)
    {
        for(int y = 0; y <= countY; ++y)
        {
            if(y == halfCountY)
            {
                if(x == halfCountX)
                {
                    std::cout << "+";
                    continue;
                }

                if(x == -1)
                {
                    std::cout << "^";
                    continue;
                }
            }

            if(x == halfCountX)
                std::cout << "-";
            else
            {
                if(y == halfCountY)
                {
                    std::cout << "|";
                    break;
                }
                else
                    std::cout << " ";
            }
        }

        if(x == halfCountX) std::cout << ">";
        std::cout << "\n";
    }

    return 0;
}