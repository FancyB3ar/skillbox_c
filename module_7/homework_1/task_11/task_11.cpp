#include <iostream>

int main(int argc, char **argv)
{
    int countLines = 0;
    do
    {
        std::cout << "Введите какое количество строк треугольника Паскаля хотите увидеть: ";
        std::cin >> countLines;
    } while(countLines < 0);

    for(int i = 0; i < countLines; ++i)
    {
        for(int s = countLines - i; s > 1; --s) std::cout << " ";

        for(int j = 0; j <= i; ++j)
        {
            int sum = 1;
            
            for(int k = j + 1; k <= i; ++k) sum *= k;
            for(int k = 1; k < (i - j + 1); ++k) sum /= k;

            std::cout << sum << " ";
        }

        std::cout << "\n";
    }

    return 0;
}