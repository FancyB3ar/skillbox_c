#include <iostream>

int main(int argc, char **argv)
{
    int w = 0, h = 0;
    do
    {
        std::cout << "Введите ширину и высоту рамки: ";
        std::cin >> w >> h;
    } while (w < 1 || h < 2);
    
    std::cout << "|";
    for(int i = 0; i < w; ++i) std::cout << "-";
    std::cout << "|\n";

    for(int i = 1; i < h-1; ++i)
    {
        std::cout << "|";
        for(int i = 0; i < w; ++i) std::cout << " ";
        std::cout << "|\n";
    }

    std::cout << "|";
    for(int i = 0; i < w; ++i) std::cout << "-";
    std::cout << "|\n";

    return 0;
}