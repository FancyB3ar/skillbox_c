#include <iostream>

int main(int argc, char **argv)
{
    int countSymbolsOfHeader = 0;
    int countExclamationPoints = 0;

    do
    {
        std::cout << "Введите количество символов колонтитула и количество восклицательных знаков: ";
        std::cin >> countSymbolsOfHeader >> countExclamationPoints;
    } while(countSymbolsOfHeader < 1 || 
            countExclamationPoints > countSymbolsOfHeader);
    
    int countSymbolsBeforeExclamationPoints = (countSymbolsOfHeader - countExclamationPoints) / 2;

    for(int i = 0; i < countSymbolsOfHeader; ++i)
    {
        if(i < countSymbolsBeforeExclamationPoints || 
           i >= countSymbolsBeforeExclamationPoints + countExclamationPoints)
        {
            std::cout <<"~";
        }
        else
            std::cout <<"!";
    }

    std::cout << "\n";
    return 0;
}