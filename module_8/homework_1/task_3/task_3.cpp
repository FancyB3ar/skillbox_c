#include <iostream>

int main(int argc, char **argv)
{
    int minHP = 0,
        maxHP = 1;
    int minMagicResistance = 0,
        maxMagicResistance = 1;
    int minFireballPower = 0,
        maxFireballPower = 1;

    float hp = 0.f,
          magicResistance = 0.f;
    
    bool err = false;

    // Цикл считывания очков жизни и сопротивления урону магии орка
    do
    {
        err = false;

        std::cout << "Введите количество очков здоровья и сопротивляемости магии орка: ";
        std::cin >> hp >> magicResistance;

        if(hp < minHP || hp > maxHP)
        {
            std::cerr << "Количество очков жизни орка должно быть в диапазоне от " 
                      << minHP 
                      << " до " 
                      << maxHP 
                      <<" включительно!\n";
            err = true;
            continue;
        }
        
        if(magicResistance < minMagicResistance || magicResistance > maxMagicResistance)
        {
            std::cerr << "Количество очков сопротивляемость магии орка должно быть в диапазоне от " 
                      << minMagicResistance 
                      << " до " 
                      << maxMagicResistance 
                      <<" включительно!\n";
            err = true;
            continue;
        }
    } while(err);

    // Проверка на жизнь орка
    if(hp == 0.0f)
    {
        std::cout << "Упс, Вам подсунили мёртвого орка, наши соболезнования.\n";
        return 0;
    }

    // Проверка на возможность убить орка
    if(magicResistance == 1.0f)
    {
        std::cout << "Вашего орка невозможно убить, он супер сопротивляем к уронам магии.\n";
        return 0;
    }

    float fireballPower = 0.f;

    // Цикл игры
    do
    {
        std::cout << "--- Удар ---\n";

        // Цикл считывания очков мощности огненного шара
        do
        {
            err = false;

            std::cout << "Введите количество очков мощности огненного шара: ";
            std::cin >> fireballPower;

            if(fireballPower < minFireballPower || fireballPower > maxFireballPower)
            {
                std::cerr << "Количество очков мощности огненного шара должно быть в диапазоне от " 
                        << minFireballPower 
                        << " до " 
                        << maxFireballPower 
                        <<" включительно!\n";
                err = true;
                continue;
            }
        } while (err);
        
        float impactDamage = fireballPower - (fireballPower * magicResistance);
        if(impactDamage < 0.0f) impactDamage = 0.0f;
        
        hp = (hp - impactDamage < 0.0f)?(0.0f):(hp - impactDamage);

        std::cout << "Итоговый урон = " 
                  << impactDamage 
                  << " от удара.\n" 
                  << "Оставшиеся очки здоровья орка = " 
                  << hp 
                  << ".\n";

        std::cout << "------------\n\n";
    } while(hp > 0.0f);

    std::cout << "Сожалеем, Ваш орк пал в бою!\nGAME OVER\n";

    return 0;
}
