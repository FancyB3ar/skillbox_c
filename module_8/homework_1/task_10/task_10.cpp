#include <iostream>


int main(int argc, char **argv)
{
    double maxLevelDangerous = 1,
           deep = 0;
    double currentLevelDangerous = 0;
    double maxDeep = 4,
           minDeep = 0;

    std::cout << "Введите максимально допустимый уровень опасности: ";
    std::cin >> maxLevelDangerous;

    do
    {
        deep = (maxDeep - minDeep) / 2 + minDeep;

        currentLevelDangerous = deep * deep * deep - 3 * deep * deep - 12 * deep + 10;

        // Если посчитанный уровень опасности больше допустимого
        if(std::abs(currentLevelDangerous) > maxLevelDangerous)
        {
            // Если посчитанный уровень опасности
            if(currentLevelDangerous < 0)
                maxDeep = deep;
            else
                minDeep = deep;
        }
        else
        {
            break;
        }
    }
    while(true);

    std::cout << "Приблизительная глубина безопасной кладки: " << deep << std::endl;

    return 0;
}
