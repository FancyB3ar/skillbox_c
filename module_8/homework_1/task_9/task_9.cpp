#include <iostream>

int main(int argc, char **argv)
{
    const float attenuation = (100 - 8.4) / 100;
    
    // Начальная и конечная амплитуды колебания маятника
    float begin,
          end;

    do
    {
        std::cout << "Введите начальную амплитуду колебания в сантиметрах\n"
                  << "и конечную амплитуду его колебаний,\n"
                  << "которая считается остановкой маятника: ";
        std::cin >> begin >> end;
    }while(begin <= 0 || end <= 0);

    int i = 0; // Количество качаний маятника до его предполагаемой остановки

    while(begin > end)
    {
        ++i;
        begin *= attenuation;
    }

    std::cout << i
              << " раз качнётся маятник, прежде чем он остановится.\n";

    return 0;
}