#include <iostream>


/*
                        x
 (au, 0) |---------------->
         |
         |
         |
         |
         |
         |
       y V
*/


int main(int argc, char **argv)
{
    const float au = 1.496e+11f;

    double posXmin = 0.001f * au,
          posYmin = 0.f;
    double posXmax = posXmin + 15.f,
          posYmax = 20.f;

    double posX = posXmin,
          posY = posYmin;

    char command = '\0';

    while(true)
    {
        std::cout << "Марсоход находится на позиции " << std::fixed << posX << ", " << std::fixed << posY << ", введите команду: \n";
        std::cin >> command;

        switch(command)
        {
            case 'w':
                if(posX > posXmin) posX -= 1.f;
                break;

            case 'a':
                if(posY > posYmin) posY -= 1.f;
                break;

            case 's':
                if(posX < posXmax) posX += 1.f;
                break;

            case 'd':
                if(posY < posYmax) posY += 1.f;
                break;

            default:
                break;
        }
    }

    return 0;
}
