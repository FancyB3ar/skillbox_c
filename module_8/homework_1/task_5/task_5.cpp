#include <iostream>
#include <cmath>

int main(int argc, char **argv)
{
    float sizeFile = 0.f, speedConnection = 0.f;
    bool err = false;

    do
    {
        err = false;

        std::cout << "Укажите размер файла для скачивания: ";
        std::cin >> sizeFile;

        if(sizeFile < 0.0f)
        {
            err = true;
            std::cerr << "Размер файла должен быть больше нуля!\n";
            continue;
        }

        std::cout << "Какова скорость вашего соединения? ";
        std::cin >> speedConnection;

        if(speedConnection < 0.0f)
        {
            err = true;
            std::cerr << "Скорость соединения должна быть больше нуля!\n";
            continue;
        }
    } while(err);

    float downloaded = 0.0f,
          stepPercent = 100.0f / sizeFile;
    int percent = 0;

    for(int i = 1; downloaded < sizeFile; ++i)
    {
        downloaded = i * speedConnection;
        if(downloaded > sizeFile) downloaded = sizeFile;

        percent = static_cast<int>(round(stepPercent * downloaded));
        if(percent > 100) percent = 100;
        
        std::cout << "Прошло " 
                  << i 
                  << " сек. Скачано " 
                  << downloaded 
                  << " из " 
                  << sizeFile 
                  << " Мб (" 
                  << percent 
                  << "%).\n";
    }

    return 0;
}