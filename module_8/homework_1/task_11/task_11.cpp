#include <iostream>
#include <cmath>

int main(int argc, char **argv)
{
    float a = 0.0f,
          b = 0.0f;

    std::cout << "Введите два числа: ";
    std::cin >> a >> b;

    std::cout << "Максимальное среди них " 
              << (std::abs(a - b) + a + b) / 2
              << ".\n";

    return 0;
}