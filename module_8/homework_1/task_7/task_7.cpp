#include <iostream>
#include <cmath>

int main(int argc, char **argv)
{
    int countKM = 0;

    do
    {
        std::cout << "Привет, Сэм! Сколько километров ты сегодня пробежал? ";
        std::cin >> countKM;
    }while(countKM < 0);

    // Переменная для вычисления среднего темпа бегуна
    int sumCircles = 0;

    for(int i = 1; i <= countKM; ++i)
    {
        int lapTime = 0;

        do
        {
            std::cout << "Какой у тебя был темп на километре " << i << "? ";
            std::cin >> lapTime;
        }while(sumCircles < 0);

        sumCircles += lapTime;
    }

    if(countKM)
    {
        // Считаем средний темп в минутах
        float averagePace = static_cast<float>(sumCircles) / countKM / 60;

        std::cout << "Твой средний темп за тренировку: "
                  << static_cast<int>(averagePace)
                  << " минут "
                  << round((averagePace - static_cast<int>(averagePace)) * 60)
                  << " секунд.\n";
    }
    else
    {
        std::cout << "Нельзя одновременно пропускать занятия по бегу и учёбу!\n";
    }

    return 0;
}