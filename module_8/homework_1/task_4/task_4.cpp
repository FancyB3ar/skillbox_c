#include <iostream>
#include <cmath>

int main(int argc, char **argv)
{
    float x = 0.f, y = 0.f, z = 0.f;
    bool err = false;

    do
    {
        err = false;

        std::cout << "Введите размеры бруска в сантиметрах.\n";

        std::cout << "X: ";
        std::cin >> x;
        
        if(x < 0)
        {
            err = true;
            std::cerr << "Размер не может быть меньше нуля!\n";
            continue;
        }

        std::cout << "Y: ";
        std::cin >> y;

        if(y < 0)
        {
            err = true;
            std::cerr << "Размер не может быть меньше нуля!\n";
            continue;
        }

        std::cout << "Z: ";
        std::cin >> z;

        if(z < 0)
        {
            err = true;
            std::cerr << "Размер не может быть меньше нуля!\n";
            continue;
        }
    } while(err);

    if(x < 5.0f || y < 5.0f || z < 5.0f)
    {
        std::cout << "Не получится сделать ни одного кубика со сторонами 5 см из такого бруска.\n";
        return 0;
    }

    int countBlock = static_cast<int>(x/5)*static_cast<int>(y/5)*static_cast<int>(z/5);
    std::cout << "Из этого бруска можно изготовить "
              << countBlock 
              << " кубиков.\n"
              << "Из них можно составить набор из " 
              << pow(static_cast<int>(pow(countBlock, 1.0 / 3.0)), 3) 
              << " кубиков.\n";
    return 0;
}