#include <iostream>

int main()
{
	int growthDay,
		consumeNight,
		startHeight,
		ripeningHeight;

	std::cout << "Мечта ботаника!\n\n";
	std::cout << "Введите прирост саженца за день в сантиметрах: ";
	std::cin >> growthDay;

	std::cout << "Введите количество сантиметров съедаемых за ночь гусеницами: ";
	std::cin >> consumeNight;

	std::cout << "Введите высоту саженца в сантиметрах при посадке: ";
	std::cin >> startHeight;

	std::cout << "Введите высоту созревания бамбука в сантиметрах: ";
	std::cin >> ripeningHeight;

	int countDay = (ripeningHeight - startHeight) / (growthDay - consumeNight);

	std::cout << "Бамбук созреет через "
			  << countDay
			  << " дней.\n";
}