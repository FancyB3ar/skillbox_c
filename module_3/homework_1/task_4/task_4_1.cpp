#include <iostream>

int main()
{
	int growthDay,
		consumeNight,
		startHeight,
		countDay;

	std::cout << "Мечта ботаника!\n\n";
	std::cout << "Введите прирост саженца за день в сантиметрах: ";
	std::cin >> growthDay;

	std::cout << "Введите количество сантиметров съедаемых за ночь гусеницами: ";
	std::cin >> consumeNight;

	std::cout << "Введите высоту саженца в сантиметрах при посадке: ";
	std::cin >> startHeight;

	std::cout << "Введите количество дней, через которое нужно знать роста бамбука: ";
	std::cin >> countDay;

	int growthInXDays = (growthDay - consumeNight) * (countDay -1),
		growthInHalfDay = growthDay / 2,
		height = startHeight + growthInXDays + growthInHalfDay;

	std::cout << "К середине "
			  << countDay
			  << " дня бамбук будет "
			  << height
			  << " см.\n";
}