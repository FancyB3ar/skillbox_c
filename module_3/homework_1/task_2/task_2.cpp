#include <iostream>

int main()
{
	int countPassengers = 0,
		countPassengersEnter,
		countPassengersExit,
		priceTicket = 20,
		total = 0;

	std::cout << "Начнём наш маршрут!\n\nПрибываем на остановку Улица Программистов. В салоне пассажиров: "
			  << countPassengers
			  << "\n";
	std::cout << "Сколько пассажиров вышло на остановке? 0\n";
	std::cout << "Сколько пассажиров зашло на остановке? ";
	std::cin >> countPassengersEnter;
	total += countPassengersEnter * priceTicket;
	countPassengers += countPassengersEnter;
	std::cout << "Отправляемся с остановки Улица Программистов. В салоне пассажиров: "
			  << countPassengers << "\n";

	std::cout << "-----------Едем---------\nПрибываем на остановку Проспект Алгоритмов. В салоне пассажиров: "
			  << countPassengers
			  << "\n";
	std::cout << "Сколько пассажиров вышло на остановке? ";
	std::cin >> countPassengersExit;
	// нужна проверка на количество выходящих пассажиров, но условия ещё не проходили
	countPassengers -= countPassengersExit;
	std::cout << "Сколько пассажиров зашло на остановке? ";
	std::cin >> countPassengersEnter;
	total += countPassengersEnter * priceTicket;
	countPassengers += countPassengersEnter;
	std::cout << "Отправляемся с остановки Проспект Алгоритмов. В салоне пассажиров: "
			  << countPassengers << "\n";

	std::cout << "-----------Едем---------\nПрибываем на остановку Pig Data. В салоне пассажиров: "
			  << countPassengers
			  << "\n";
	std::cout << "Сколько пассажиров вышло на остановке? ";
	std::cin >> countPassengersExit;
	// нужна проверка на количество выходящих пассажиров, но условия ещё не проходили
	countPassengers -= countPassengersExit;
	std::cout << "Сколько пассажиров зашло на остановке? ";
	std::cin >> countPassengersEnter;
	total += countPassengersEnter * priceTicket;
	countPassengers += countPassengersEnter;
	std::cout << "Отправляемся с остановки Pig Data. В салоне пассажиров: "
			  << countPassengers << "\n";

	std::cout << "-----------Едем---------\nПрибываем на остановку Всемирной паутины. В салоне пассажиров: "
			  << countPassengers
			  << "\n";
	std::cout << "Сколько пассажиров вышло на остановке? ";
	std::cin >> countPassengersExit;
	// нужна проверка на количество выходящих пассажиров, но условия ещё не проходили
	countPassengers -= countPassengersExit;
	std::cout << "Сколько пассажиров зашло на остановке? ";
	std::cin >> countPassengersEnter;
	total += countPassengersEnter * priceTicket;
	countPassengers += countPassengersEnter;
	std::cout << "Отправляемся с остановки Всемирной паутины до конечной остановки Бесконечный путь. В салоне пассажиров: "
			  << countPassengers << "\n\nМаршрут закончен!\n\n";


	int salary = total / 4,
		sumPetrol = total / 5,
		sumTax = total / 5,
		sumRepair = total / 5;

	std::cout << "Всего заработали: "
			  << total
			  << " руб.\nЗарплата водителя: "
			  << salary
			  << " руб.\nРасходы на топливо: "
			  << sumPetrol
			  << " руб.\nНалоги: "
			  << sumTax
			  << " руб.\nРасходы на ремонт машины: "
			  << sumRepair
			  << " руб.\nИтого доход: "
			  << total - (salary + sumPetrol + sumTax + sumRepair)
			  << " руб.\n";
}