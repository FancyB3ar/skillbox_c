#include <iostream>

int main() {
	int costProduct, costDelivery, sizeDiscountMoney, result;

	std::cout << "Приветствуем вас в калькуляторе!\nОн поможет рассчитать полную стоимость товаров с учетом скидок и доставки.\n";
	std::cout << "Введите стоимость товара: ";
	std::cin >> costProduct;
	std::cout << "Введите стоимость доставки: ";
	std::cin >> costDelivery;
	std::cout << "Введите размер скидки: ";
	std::cin >> sizeDiscountMoney;
	std::cout << "-----Считаем-----\n";
	result = costProduct + costDelivery - sizeDiscountMoney;
	std::cout << "Итого: " << result << "\n";
}