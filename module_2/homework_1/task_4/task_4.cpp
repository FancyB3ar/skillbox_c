#include <iostream>

int main() {
	int amountReceipt, countPorch, countApartment, result;

	std::cout << "Приветствуем вас в калькуляторе квартплаты!\n\n";
	std::cout << "Введите сумму, указанную в квитанции: ";
	std::cin >> amountReceipt;
	std::cout << "Сколько подъездов в вашем доме? ";
	std::cin >> countPorch;
	std::cout << "Сколько квартир в каждом подъезде? ";
	std::cin >> countApartment;
	std::cout << "-----Считаем-----\n";
	result = amountReceipt / (countPorch * countApartment);
	std::cout << "Каждая квартира должна заплатить по " << result << " руб.\n";
}