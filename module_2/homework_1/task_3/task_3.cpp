#include <iostream>

int main() {
	int durationShiftMinutes, durationMakeOrder, durationCollectOrder, countOrderPerShift;

	std::cout << "Эта программа рассчитает, сколько клиентов успеет обслужить кассир за смену.\n";
	std::cout << "Введите длительность смены в минутах: ";
	std::cin >> durationShiftMinutes;
	std::cout << "Сколько минут клиент делает заказ? ";
	std::cin >> durationMakeOrder;
	std::cout << "Сколько минут кассир собирает заказ? ";
	std::cin >> durationCollectOrder;
	std::cout << "-----Считаем-----\n";
	countOrderPerShift = durationShiftMinutes / (durationMakeOrder + durationCollectOrder);
	std::cout << "За смену длиной " << durationShiftMinutes << " минут кассир успеет обслужить " << countOrderPerShift << " клиентов\n";
}