#include <iostream>

int main()
{
	int a1, b1, a2, b2, a3, b3;
    bool flagOpportunity = true;

	std::cout << "Введите координаты первой точки: ";
	std::cin >> a1 >> b1;

	std::cout << "Введите координаты второй точки: ";
	std::cin >> a2 >> b2;

	std::cout << "Введите координаты третьей точки: ";
	std::cin >> a3 >> b3;
    
    // Блок проверки нахождения точек на одной прямой не равных друг другу
    if( (a3 * (b2 - b1)) - (b3 * (a2 - a1)) == a1 * b2 - a2 * b1)
    {
        flagOpportunity = false;
    }

    if(flagOpportunity)
        std::cout << "Yes\n";
    else
        std::cout << "No\n";

    return 0;
}