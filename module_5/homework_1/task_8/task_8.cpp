#include <iostream>

int main()
{
    int currentDay, currentMonth, currentYear;
    int dayOfBirth, monthOfBirth, yearOfBirth;
    bool flagOpportunity = false;
    int ageOfMajority = 18;

	std::cout << "Введите текущий день, месяц и год: ";
	std::cin >> currentDay >> currentMonth >> currentYear;

    if(currentDay > 31 || currentDay < 1 || currentMonth > 12 || currentMonth < 1)
    {
        std::cout << "Проверьте корректность введённой даты.\n";
        return 2;
    }

    std::cout << "Введите день, месяц и год рождения клиента: ";
	std::cin >> dayOfBirth >> monthOfBirth >> yearOfBirth;

    if(dayOfBirth > 31 || dayOfBirth < 1 || monthOfBirth > 12 || monthOfBirth < 1)
    {
        std::cout << "Проверьте корректность введённой даты.\n";
        return 2;
    }

    if(currentYear > yearOfBirth + ageOfMajority)
    {
        flagOpportunity = true;
    }
    else
    {
        if(currentYear == yearOfBirth + ageOfMajority)
        {
            if(currentMonth > monthOfBirth)
            {
                flagOpportunity = true;
            }
            else
            {
                if(currentMonth == monthOfBirth && currentDay > dayOfBirth)
                {
                    flagOpportunity = true;
                }
            }
        }
    }

    if(flagOpportunity)
    {
        std::cout << "Пускай пьёт что хочет.\n";
        return 0;
    }
    else
    {
        std::cout << "Нельзя ему продавать алкоголь, пускай пьёт молоко.\n";
        return 1;
    }
}