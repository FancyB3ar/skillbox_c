#include <iostream>

int main()
{
	int a, b, c;
    int m, n, k;

	std::cout << "Введите длину, широты и высоту первой коробки: ";
	std::cin >> a >> b >> c;

    if(a <= 0 || b <= 0 || c <= 0)
    {
        std::cout << "Размеры коробки не могут быть меньше либо равны нулю.\n";
        return 2;
    }

    std::cout << "Введите длину, широты и высоту второй коробки: ";
	std::cin >> m >> n >> k;

    if(m <= 0 || n <= 0 || k <= 0)
    {
        std::cout << "Размеры коробки не могут быть меньше либо равны нулю.\n";
        return 2;
    }

    bool flagOpportunity = false;

    if( (a <= m && b <= n && c <= k) || (b <= m && a <= n && c <= k) )
    {
        flagOpportunity = true;
    }
    else
    {
        if( (c <= m && b <= n && a <= k) || (b <= m && c <= n && a <= k) )
        {
            flagOpportunity = true;
        }
        else
        {
            if( (a <= m && c <= n && b <= k) || (c <= m && a <= n && b <= k) )
            {
                flagOpportunity = true;
            }
        }
    }

    if(flagOpportunity)
    {
        std::cout << "Первая коробка поместится во второй.\n";
        return 0;
    }
    else
    {
        std::cout << "Первая коробка не поместится во второй.\n";
        return 1;
    }
}