// Пример в задаче: "Например, 800 рублей можно выдать как 4 по 200 или как 500 и еще 3 по 100."
// не совсем корректен, так как по условиям у нас нет ограничений кол-ва купюр в банкомате, 
// а значит 800 рублей раскладывается минимальным количеством купюр не по 4 купюры двумя способами, 
// а одним способом в 3 купюры (500 -1, 200 -1, 100 -1).

#include <iostream>

int main()
{
    int count = 0;
    int maxCountPerTransaction = 150000;
    
    std::cout << "Введите размер снимаемой суммы: ";
	std::cin >> count;

    if(count > maxCountPerTransaction)
    {
        std::cout << "Превышена запрашиваемая сумма операции, попробуйте уменьшить сумму.\n";
        return 1;
    }

    if(count % 100)
    {
        std::string answer = "n";

        if(count / 100)
        {
            std::cout << "Запрашиваемая сумма не может быть выдана полностью, хотите получить "
                      << count - count % 100 << " рублей? [y/n] ";
            std::cin >> answer;
        }
        else
        {
            std::cout << "Запрашиваемая сумма не может быть выдана!\n"
                      << "Минимальная сумма выдачи 100 рублей.\n";
        }

        if(answer != "y")
        {
            std::cout << "Операция отклонена пользователем.\n";
            return 2;
        }
    }

    // Блок подсчёта кол-ва 5000 купюр
    {
        int nominalCost = 5000;
        std::cout << nominalCost << " - " << count / nominalCost << "\n";
        count%=nominalCost;
    }

    // Блок подсчёта кол-ва 2000 купюр
    {
        int nominalCost = 2000;
        std::cout << nominalCost << " - " << count / nominalCost << "\n";
        count%=nominalCost;
    }

    // Блок подсчёта кол-ва 1000 купюр
    {
        int nominalCost = 1000;
        std::cout << nominalCost << " - " << count / nominalCost << "\n";
        count%=nominalCost;
    }

    // Блок подсчёта кол-ва 500 купюр
    {
        int nominalCost = 500;
        std::cout << nominalCost << "  - " << count / nominalCost << "\n";
        count%=nominalCost;
    }

    // Блок подсчёта кол-ва 200 купюр
    {
        int nominalCost = 200;
        std::cout << nominalCost << "  - " << count / nominalCost << "\n";
        count%=nominalCost;
    }

    // Блок подсчёта кол-ва 100 купюр
    {
        int nominalCost = 100;
        std::cout << nominalCost << "  - " << count / nominalCost << "\n";
        count%=nominalCost;
    }
}