#include <iostream>

int main()
{
	int speedOfPlane, heightOfPlane;
	
	std::cout << "Введите скорость (км/ч) и высоту (м) полёта самолёта: ";
	std::cin >> speedOfPlane >> heightOfPlane;

	if(speedOfPlane >= 750 && speedOfPlane < 850 && heightOfPlane >= 9000 && heightOfPlane < 9500)
	{
		std::cout << "Самолёт летит правильным эшелоном.\n";
	}
	else
	{
		std::cout << "Самолёт летит неправильным эшелоном.\n";
	}
}