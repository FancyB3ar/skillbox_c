#include <iostream>

int main()
{
	int numDay, dayOfWeek, differentDays;

	std::cout << "Введите номер дня в мае и номер дня недели с которого начинается месяц: ";
	std::cin >> numDay >> dayOfWeek;

    differentDays = numDay + dayOfWeek - 1;

    // Проверяем номер дня в мае
    if(numDay < 1 || numDay > 31)
    {
        std::cout << "Проверьте вводимые данные, такого номера дня нет в мае.\n";
        return 1;
    }

    // Проверяем номер дня недели с которого начинается месяц
    if(dayOfWeek < 1 || dayOfWeek > 7)
    {
        std::cout << "Проверьте вводимые данные, такого номера дня недели нет.\n";
        return 2;
    }

    if( (numDay <= 5) || (numDay >= 8 && numDay <= 10) || !(differentDays % 7 % 6) )
    {
        std::cout << "Можете отдыхать этот день выходной.\n";
    }
    else
    {
        std::cout << "Лучший день чтобы поехать на работу.\n";
    }

    return 0;
}