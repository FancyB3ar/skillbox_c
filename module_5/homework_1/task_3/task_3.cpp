#include <iostream>

int main()
{
	int numDay;

	std::cout << "Введите номер дня в мае: ";
	std::cin >> numDay;

    if(numDay < 1 || numDay > 31)
    {
        std::cout << "Проверьте вводимые данные, такого номера дня нет в мае.\n";
        return 1;
    }

    if( numDay < 11 || !(numDay % 7 % 6) )
    {
        std::cout << "Можете отдыхать этот день выходной.\n";
    }
    else
    {
        std::cout << "Лучший день чтобы поехать на работу.\n";
    }

    return 0;
}