#include <iostream>

int main()
{
	int count, modulo100;
    std::string valuta;

	std::cout << "Введите число: ";
	std::cin >> count;

    modulo100 = count % 100;

    if(modulo100 < 11 || modulo100 > 19)
    {
        int modulo10 = count % 10;

        if(modulo10 == 1)
        {
            valuta = "рубль";
        }
        else
        {
            if(modulo10 > 1 && modulo10 < 5)
            {
                valuta = "рубля";
            }
            else
            {
                valuta = "рублей";
            }
        }
    }
    else
    {
        valuta = "рублей";
    }
    
    std::cout << count << " " << valuta << "\n";

    return 0;
}