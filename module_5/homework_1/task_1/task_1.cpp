#include <iostream>

int main()
{
	int volumeOfAlcohol, countMedicineChest;
	
	std::cout << "Введите число мл медицинского спирта и наборов походных аптек: ";
	std::cin >> volumeOfAlcohol >> countMedicineChest;

	if(volumeOfAlcohol < 2000 && countMedicineChest < 5)
	{
		std::cout << "Снаряжения у фельдшера для выхода на работу с ранеными недостаточно.\n";
	}
	else
	{
		std::cout << "Снаряжения у фельдшера для выхода на работу с ранеными достаточно.\n";
	}
}