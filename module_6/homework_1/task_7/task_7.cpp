#include <iostream>

int main()
{
    int index = 0;
    uint num1 = 1,
        num2 = 1;
    uint result = 1;

    do
    {
        std::cout << "Введите индекс числа Фибоначчи: ";
        std::cin >> index;
    } while(index < 1 || index > 47);
    // 47 последний индекс при котором число допустимо для типа uint

    for(int i = 2; i < index; ++i)
    {
        result = num1 + num2;
        num1 = num2;
        num2 = result;
    }

    std::cout << "Число Фибоначчи по индексу " << index << " = " << result << "\n";
    return 0;
}