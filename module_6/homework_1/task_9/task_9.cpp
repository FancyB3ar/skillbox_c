#include <iostream>

int main()
{
    int x, y, p;
    std::cout << "Введите размер вклада, желаемый размер вклада и процент в год: ";
    std::cin >> x >> y >> p;

    if(x <= 0)
    {
        std::cout << "Ваш вклад должен быть больше 0, чтобы что-то возможно было накопить.\n";
        return 1;
    }

    if(y <= x)
    {
        std::cout << "Вы уже имеете необходимую сумму.\n";
        return 0;
    }

    std::cout << "Введите \"y\" если вклад с капитализацией или \"n\" если без: ";
    std::string capitalisation = "";

    do
    {
        std::cin >> capitalisation;
    } while(capitalisation != "y" && capitalisation != "n");

    int countYears = 0;
    int deposit = x;

    while(y > deposit)
    {
        int rubles = 0;

        if(capitalisation == "y")
        {
            rubles = (deposit / 100 * p) + (deposit % 100 * p / 100);
        }
        else
        {
            rubles = (x / 100 * p) + (x % 100 * p / 100);
        }

        if(!rubles)
        {
            std::cout << "Из-за того что наш банк присваивает себе Ваши копейки,\n"
                      << "Вы никогда не накопите желаемую сумму вклада с таким процентом.\n";
            return 0;
        }

        deposit += rubles;
        
        ++countYears;
    }

    std::cout << "Необходимую сумму Вы накопите через " << countYears << " лет.\n";
    return 0;
}