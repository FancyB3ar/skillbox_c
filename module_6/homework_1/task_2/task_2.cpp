#include <iostream>

int main()
{
    int debt = 0;
    int giveMoney = 0;
    std::string name = "";

    std::cout << "Введите имя должника: ";
    std::cin >> name;

    do
    {
        std::cout << "Введите размер долга (число должно быть больше 0): ";
        std::cin >> debt;
    } while (debt <= 0);
 
    do
    {
        std::cout << name << ", ваша задолженность составляет " << debt << " рублей.\n"
        << "Сколько рублей вы внесёте прямо сейчас, чтобы её погасить? ";
        std::cin >> giveMoney;
    } while (debt > giveMoney);
    
    std::cout << "Долг погашен!\n";
    return 0;
}