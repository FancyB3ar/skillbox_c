#include <iostream>

int main()
{
    int maxNum = 64;
    int findNum = maxNum / 2;
    std::string answer = "";

    std::cout << "Начинаем игру, загадайте число от 0 до 63, а я постараюсь его отгадать.\n";

    do
    {
        std::cout << "Ваше число больше " << findNum << "? [y/n] ";
        
        do
        {
            std::cin >> answer;
        } while(answer != "y" && answer != "n");
        
        if(answer == "y")
        {
            int diff = (maxNum - findNum) / 2;
            
            if(diff == 0)
            {
                std::cout << "Вы где-то обманули!\n";
                return 1;
            }

            findNum += diff;
        }
        else
        {
            std::cout << "Ваше число меньше " << findNum << "? [y/n] ";
        
            do
            {
                std::cin >> answer;
            } while (answer != "y" && answer != "n");

            if(answer == "y")
            {
                int diff = (maxNum - findNum) / 2;
            
                if(diff == 0)
                {
                    std::cout << "Вы где-то обманули!\n";
                    return 1;
                }

                int maxNum_new = findNum;
                findNum -= diff;
                maxNum = maxNum_new;
            }
            else
            {
                std::cout << "Ваше число " << findNum << ".\n";
                return 0;
            }
        }
    } while(findNum > 1);
    
    std::cout << "Ваше число 1? [y/n] ";
    
    do
    {
        std::cin >> answer;
    } while(answer != "y" && answer != "n");
    
    if(answer == "y") std::cout << "Ваше число 1.\n";
    else std::cout << "Ваше число 0.\n";

    return 0;
}