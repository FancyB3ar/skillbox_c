#include <iostream>

int main()
{
    int num = 0;

    std::cout << "Введите число: ";
    std::cin >> num;

    if(num <= 0)
    {
        std::cout << "Не степень 2.\n";
        return 1;
    }

    if((num & (num - 1)) == 0)
    {
        std::cout << "Степень 2.\n";
    }
    else
    {
        std::cout << "Не степень 2.\n";
    }

    return 0;
}