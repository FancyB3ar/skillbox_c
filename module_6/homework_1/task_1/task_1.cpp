#include <iostream>

int main()
{
    int hour = 0;
    std::cout << "Введите номер часа для кукушки: ";
    std::cin >> hour;

    if(hour < 0 || hour > 24)
    {
        std::cout << "Номер часа не может быть меньше 0 и больше 24.\n";
        return 1;
    }

    for(int i = 0; i < hour; ++i)
    {
        std::cout << (i + 1) << ") Ку-ку!\n";
    }

    return 0;
}