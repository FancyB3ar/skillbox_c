#include <iostream>

int main()
{
    char c = '\0';
    int count = 0;

    std::cout << "Введите число и в конце нажмите Enter: ";

    while(std::cin.get(c) && c != '\n')
    {
        ++count;
    }
    
    std::cout << "Кол-во символов: " << count << "\n";
}