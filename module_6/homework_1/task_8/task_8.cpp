#include <iostream>

int main()
{
    int m = 0,
        n = 0;
    std::cout << "Введите числитель и знаменатель: ";
    std::cin >> m >> n;

    // НОД
    int a = m, b = n;

    while(a != b)
    {
        if(a > b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }

        b = b - a;
    }

    std::cout << m/a << "/" << n/a << "\n";
    return 0;
}