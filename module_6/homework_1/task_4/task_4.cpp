#include <iostream>

int main()
{
    int debt = 0;
    int giveMoney = 0;
    std::string name = "";

    std::cout << "Введите имя должника: ";
    std::cin >> name;

    do
    {
        std::cout << "Введите размер долга (число должно быть больше 0): ";
        std::cin >> debt;
    } while (debt <= 0);
 
    do
    {
        giveMoney = 0;
        std::cout << name << ", ваша задолженность составляет " << debt << " рублей.\n"
        << "Сколько рублей вы внесёте прямо сейчас, чтобы её погасить? ";
        std::cin >> giveMoney;
        
        if(giveMoney <= 0) continue;
        
        debt -= giveMoney;
    } while (debt > giveMoney);
    
    if(debt < 0)
    {
        std::cout << "Ваш остаток: " << (-debt) << " рублей.\n";
    }

    std::cout << "Долг погашен!\n";
    return 0;
}