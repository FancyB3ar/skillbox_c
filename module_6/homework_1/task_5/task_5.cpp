#include <iostream>

int main()
{
    int num = 0;

    std::cout << "Введите номер билета: ";
    std::cin >> num;

    while(num < 0 || num > 999999)
    {
        std::cout << "Неправильный номер билета, повторите ввод: ";
        std::cin >> num;
    }

    int count1 = 0,
        count2 = 0;

    for(int i = 0; i < 3; ++i)
    {
        count1 += num % 10;
        num /= 10;
    }

    for(int i = 0; i < 3; ++i)
    {
        count2 += num % 10;
        num /= 10;
    }
    if(count1 == count2) std::cout << "Счастливый.\n";
    else std::cout << "Несчастливый.\n";

    return 0;
}