#include <iostream>

int main()
{
    int salary1 = 110000, 
        salary2 = 85000, 
        salary3 = 210000;

    int minSalary = salary1, 
        maxSalary = salary1;
    
    // Поиск минимальной зарплаты
    if(minSalary > salary2)
    {
        minSalary = salary2;
    }

    if(minSalary > salary3)
    {
        minSalary = salary3;
    }

    // Поиск максимальной зарплаты
    if(maxSalary < salary2)
    {
        maxSalary = salary2;
    }

    if(maxSalary < salary3)
    {
        maxSalary = salary3;
    }

    int differentSalary = maxSalary - minSalary;
    std::cout << "Разница между самой высокой (" << maxSalary 
              << ") и низкой (" << minSalary << ") зарплатами: " 
              << differentSalary << "\n";

    std::cout << "Средняя зарплата сотрудников: " 
              << (salary1 + salary2 + salary3) / 3 << "\n";
}