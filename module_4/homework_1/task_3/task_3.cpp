#include <iostream>

int main()
{
	int num;

    std::cout << "Введите число: ";
	std::cin >> num;

    std::cout << ((num % 2)?"Нечётное число":"Чётное число") << "\n";
}