#include <iostream>

int main()
{
	int summand1, summand2, amount;

    std::cout << "Введите первое слагаемое: ";
	std::cin >> summand1;

	std::cout << "Введите второе слагаемое: ";
	std::cin >> summand2;

    std::cout << "Введите сумму: ";
	std::cin >> amount;

    if(amount == summand1 + summand2)
    {
        std::cout << "Вы правильно посчитали\n";
    }
    else
    {
        std::cout << "Вы неправильно посчитали, правильный ответ " << summand1 + summand2 << "\n";
    }
}