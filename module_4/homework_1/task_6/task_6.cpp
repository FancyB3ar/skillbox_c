#include <iostream>

int main()
{
    // Кол-во мужчин в городе
    int menCount;
	std::cout << "Введите число мужчин в городе: ";
    std::cin >> menCount;

    // Кол-во барберов
    int barbersCount;
    std::cout << "Сколько барберов удалось нанять: ";
    std::cin >> barbersCount;

    // Кол-во человек которое подстрижёт барбер за смену 8 часов
    int menPerBarber = 8;
    // Кол-во человек подстрижёт барбер за месяц (бедняги без выходных)
    int menPerBarberPerMonth = menPerBarber * 30;
    std::cout << "Один барбер стрижёт " << menPerBarberPerMonth << " человек в месяц\n";

    // Кол-во барберов необходимых чтобы подстричь menCount человек за месяц
    int requiredBarbersCount = menCount / menPerBarberPerMonth;
    
    if(menCount % menPerBarberPerMonth)
    {
        requiredBarbersCount++;
    }

    std::cout << "Необходимо всего барберов: " << requiredBarbersCount << "\n";

    if(requiredBarbersCount > barbersCount)
    {
        std::cout << "Нужно больше барберов.\n";
    }
    else
    {
        std::cout << "Барберов хватает.\n";
    }
}