#include <iostream>

int main()
{
    // День недели
    int dayOfWeek;
	std::cout << "Введите номер дня недели: ";
    std::cin >> dayOfWeek;

    std::string nameDay = "";
    std::string menuOfDay = "";
    std::string baseMenu = "1. Чай / кофе\n2. Суп\n3. Салат\n4. Хлеб\n";

    switch(dayOfWeek)
    {
        case 1:
        {
            nameDay = "понедельник";
            menuOfDay = "Пюре с гуляшом.";            
            break;
        }
    
        case 2:
        {
            nameDay = "вторник";
            menuOfDay = "Гречка с отбивной.";
            break;
        }
    
        case 3:
        {
            nameDay = "среда";
            menuOfDay = "Плов.";
            break;
        }
    
        case 4:
        {
            nameDay = "четверг";
            menuOfDay = "Макароны с куриной грудкой.";
            break;
        }
    
        case 5:
        {
            nameDay = "пятница";
            menuOfDay = "Рис с эскалопом.";
            break;
        }
        
        case 6:
        {
            nameDay = "суббота";
            menuOfDay = "Фуагра.";
            break;
        }
        
        case 7:
        {
            nameDay = "воскресенье";
            menuOfDay = "Фрикасе из курицы с овощами.";
            break;
        }

        default:
        {
            std::cout << "Введён неправильный день недели, должно быть число от 1 до 7.\n";
            return 1;
        }
    }

    std::cout << "Сегодня " << nameDay << ".\n\nМеню дня:\n"
              << menuOfDay << "\n\nОсновное меню:\n" << baseMenu;

    return 0;
}