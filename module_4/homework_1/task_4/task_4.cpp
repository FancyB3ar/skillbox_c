#include <iostream>

int main()
{
	int experiencePoints = 0;
    std::string nameLevel = "";

    std::cout << "Введите число \"очков опыта\": ";
	std::cin >> experiencePoints;

    if(experiencePoints >= 5000)
    {
        nameLevel = "третий";
    }
    else
    {
        if(experiencePoints >= 2500)
        {
            nameLevel = "второй";
        }
        else
        {
            nameLevel = "первый";
        }
    }
    
    std::cout << "У Вашего персонажа " << nameLevel << " уровень\n";
}