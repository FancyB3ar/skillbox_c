#include <iostream>

int main()
{
	int divisible, divider;
	std::string strNo = "";

	std::cout << "Введите делимое: ";
	std::cin >> divisible;

	std::cout << "Введите делитель: ";
	std::cin >> divider;

    if(divisible % divider)
    {
        strNo = "не ";
    }

    std::cout << "Делимое (" << divisible << ") " << strNo << "делится на делитель (" << divider << ") без остатка\n";
}