#include <iostream>

// Лучше использовать вещественные типы для подсчёта налога, 
// иначе есть возможность получить налог меньше чем должен быть, 
// хотя так даже лучше xD
int main()
{
    int stepSalary1 = 10000, stepSalary2 = 50000;

    int salary, tax = 0;
    std::cout << "Введите размер зарплаты для расчёта налога: ";
    std::cin >> salary;

    if(salary > stepSalary2)
    {
        tax += (salary - stepSalary2) * 0.3;
        salary = stepSalary2;
    }

    if(salary > stepSalary1)
    {
        tax += (salary - stepSalary1) * 0.2;
        salary = stepSalary1;
    }

    if(salary > 0)
    {
        tax += salary * 0.13;
    }
    else
    {
        std::cout << "Самое время сменить (найти) работу и начать получать зарплату!\n";
        return 1;
    }
    
    std::cout << "Ваш налог составляет: " << tax << "\n";

    return 0;
}